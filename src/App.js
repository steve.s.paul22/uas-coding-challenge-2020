import React from 'react';
import './App.css';

import Bar from './components/Navbar';
import Table from './components/Table';

function App() {
  return (
    <div>
      <Bar />
      <Table />
    </div>
  );
}

export default App;
