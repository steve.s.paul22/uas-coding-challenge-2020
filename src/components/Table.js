import React, { Component } from 'react';
import { Container, Input, Row, Col, Table, Alert } from 'reactstrap';
import stickybits from 'stickybits'
import '../styles/Table.css';

function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}


class MultiTable extends Component {
    state = {
        nrows: 12,
        ncolumns: 15,
        prevCell: '',
        hist: {},
        isHist: false,
        tooltipOpen: false,
    }
   
    componentDidMount() {
        stickybits('.js-sticky-side-header', { stickyBitStickyOffset: 0 });
    }
    handleSizeChange = (event) => {
        this.setState({
            [event.target.name]: parseInt(event.target.value)
        });
    }
    toggle = () => {
        this.setState({ tooltipOpen: !this.state.tooltipOpen });
    }
    onCellClicked = (event, cindex, rindex) => {
        let newHist = this.state.hist;
        if (newHist.hasOwnProperty(rindex + 1)) {
            if (!newHist[rindex + 1].includes(cindex + 1)) {
                newHist[rindex + 1].push(cindex + 1)
            }

        } else {
            newHist[rindex + 1] = [cindex + 1]
        }
        console.log(newHist);
        this.setState({ prevCell: event.target, hist: newHist, isHist: true });
        const product = (cindex + 1) * (rindex + 1);
        event.target.innerHTML = product;
        if (this.state.prevCell !== '') {
            this.state.prevCell.innerHTML = '';
        }

    }
    render() {
        return (
            <Container>
                <Row>
                    <Col lg="6">
                        <Row className="no-gutters" id="sizeInfo" >
                            <Col xs="auto" className="sizeSelector">Row</Col>
                            <Col xs="auto" className="sizeSelector"><strong>:</strong></Col>
                            <Col xs="2" style={{ padding: "5px" }}>
                                <Input type="number" name="nrows" id="rowCount" value={this.state.nrows} onChange={this.handleSizeChange} />
                            </Col>
                            <Col xs="auto" className="sizeSelector">Column</Col>
                            <Col xs="auto" className="sizeSelector"><strong>:</strong></Col>
                            <Col xs="2" style={{ padding: "5px" }}>
                                <Input type="number" name="ncolumns" id="columnCount" value={this.state.ncolumns} onChange={this.handleSizeChange} />
                            </Col>
                        </Row>
                        <div style={{ height: "80vh" }}>
                            <div className="table_scroll max-w-5xl shadow-2xl ml-auto mr-auto h-full overflow-auto">
                                <div className="table bg-gray-300 relative w-full">
                                    <div className="font-bold z-10" style={{ display: "table-header-group" }}>
                                        <div className="table-row">
                                            <div colSpan="12" className="border-r border-gray-500 z-50 js-sticky-side-header css_th z-10 left-0 table-cell  p-3 p-4 border-b bg-gray-300"><strong>X</strong></div>
                                            {range(this.state.ncolumns).map((val, index) => (
                                                <div className="css_th border-r sticky top-0 table-cell  p-3 p-4 border-b border-gray-500 bg-white" key={index}>
                                                    {val + 1}
                                                </div>

                                            ))}
                                        </div>
                                    </div>
                                    <div className="css_tbody" style={{ display: "table-row-group" }}>
                                        {
                                            range(this.state.nrows).map((rval, rindex) => (

                                                <div className="css_tr table-row" key={rindex}>

                                                    <div className="border border-gray-500 css_sd sticky left-0 table-cell  p-3 p-4 bg-white" data-label="Name">
                                                        {rval + 1}
                                                    </div>

                                                    {range(this.state.ncolumns).map((cval, cindex) => (

                                                        <div
                                                            key={cindex}
                                                            className="css_td text-center table-cell  p-3 p-4 border bg-white"
                                                            data-label="Position"
                                                            onClick={(e) => this.onCellClicked(e, cindex, rindex)}

                                                        >


                                                        </div>


                                                    ))}
                                                </div>


                                            ))
                                        }


                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg="6">
                        <Container>
                            <div className="table-responsive" style={{ height: "80vh", marginTop: "62px" }} >

                                {this.state.isHist ? <Table striped dark >
                                    <thead>
                                        <tr>
                                            <th className="center-align"><h3>History</h3></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            Object.keys(this.state.hist).map((operand, ix) => (
                                                this.state.hist[operand].sort((a, b) => a - b).map((factor, i) => (
                                                    <tr key={i}>
                                                        <td  className="center-align"><strong>{operand} x {factor} = {operand * factor}</strong></td>
                                                    </tr>
                                                ))

                                            ))
                                        }

                                    </tbody>
                                </Table> :
                                    <Alert color="secondary" className="text-center">
                                        <span role="img" aria-label="pointing">👈</span> Click on any cell of the multiplication table.
                               </Alert>}


                            </div>

                        </Container>
                    </Col>
                </Row>

            </Container>
        );
    }
}

export default MultiTable;