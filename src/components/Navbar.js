import React, { Component } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
  } from 'reactstrap';

class Bar extends Component {
    state = { 
        isOpen: false,
     }

    toggle =() =>{
        this.setState({isOpen: !this.state.isOpen});
    }
    render() { 
        return ( 
            <div>
            <Navbar color="dark" dark expand="md">
              <NavbarBrand href="/">Multiplication Table</NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="mr-auto" navbar>
                  <NavItem>
                    <NavLink target="_blank" href="https://gitlab.com/steve.s.paul22/uas-coding-challenge-2020">Gitlab</NavLink>
                  </NavItem>
                </Nav>
                <NavLink target="_blank" href="https://github.com/ST2-EV">ST2-EV</NavLink>
              </Collapse>
            </Navbar>
          </div>
         );
    }
}
 
export default Bar;